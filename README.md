# Pokemon World

Pokemons are out there in a wild `world.txt`. You will have to create an application to catch them and group into
tribes.

# Requirements

- Pokemon is considered as found when in a single sentence all these components exist: `specie`, `name`, `power`
  i.e. `Vestibulum fossil at Grunt consectetur 0x29 quam.`
    - Species are static values (list can be found in [References](#References)).
    - Name cannot be a first word of a sentence, and it always starts with an upper-case letter.
    - Power points are represented in hexadecimal format.
    - You cannot find any `specie`, `name` or `power` component more than once. If you do then it's an invalid pokemon
      in a sentence and you need to ignore it.
- Count the total of true pokemons.
- Figure out which specie is the most powerful.
- Group pokemons into tribes by species.
- Pokemon with a power points number over 100 is considered as a leader.
- A tribe cannot have more than one leader. It means, the tribe should be split into few by leaders.
- If there is no natural leader for a tribe, the pokemon with the highest number of power points is assigned as a leader.

### Bonus requirements:

- If the same specie pokemons do not fill tribes equally, the pokemons with lowest power points go into tribes with
  leaders having highest power points.
- There are fake pokemons! Fake pokemon is considered: if in the pokemon name the two consecutive letters stand next to
  each other in the [English alphabet](https://en.wikipedia.org/wiki/English_alphabet).
    - Fake pokemon: `Feloor`, because the letters `e` & `F` are near by each other in both: the name & the English
      alphabet.

# Tech requirements

- Use any format to provide the results.
- Cover the solution with tests.
- Include README with clear instructions on how to build and run your solution.

# References

- Pokemon species: https://en.wikipedia.org/wiki/List_of_Pok%C3%A9mon
    - `starter`
    - `fossil`
    - `baby`
    - `legendary`
    - `mythical`
    - `ultra`
